import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        ArrayList<String> countries = new ArrayList<>();
        // add countries
        countries.add("Cambodia");
        countries.add("India");
        countries.add("Thai");
        countries.add("Loa");
        countries.add("Viet Nam");
        countries.add("USA");
        countries.add("Canada");
        countries.add("China");
        // print inserted order
        System.out.println(countries);
        for (String country: countries){
            System.out.println(country);
        }
        // print sorted manner
        Collections.sort(countries);
        System.out.println(countries);
    }
}
